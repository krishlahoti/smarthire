Graphql UI:  http://localhost:9000/graphiql

H2 DB Console: http://localhost:9000/h2/ 

Create User Query: 

mutation{
  createUser(
    login : "krishna"
    name:"Krishna Lahoti"
  )
  {
    id
  }
}

@Entity Annotation (JPA) : https://stackoverflow.com/questions/29332907/what-is-the-exact-meaning-of-the-jpa-entity-annotation

Pagination on JPA - https://www.baeldung.com/spring-data-jpa-pagination-sorting

Spring Data Sorting - https://www.baeldung.com/spring-data-sorting

ServletInitializer -> importance of this class?(doesn't pick application properties without this file included in the application)

