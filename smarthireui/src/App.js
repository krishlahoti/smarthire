import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ApolloClient from 'apollo-boost';
import {ApolloProvider, Query} from 'react-apollo';
import gql from 'graphql-tag';

const client = new ApolloClient ({
	uri: "http://localhost:9000/graphql"
});

const GET_CANDIDATE_STRING = gql`
query{
  candidates{
    id
    firstName
    lastName
    isActive
    contactNumber
  }
  
}`;



class App extends Component {
  render() {
    return (
    		<ApolloProvider client = {client}>
    		
    		<Query query = {GET_CANDIDATE_STRING} >
    		{({ loading, error, data }) => {
    		      if (loading) return <p>Loading...</p>;
    		      if (error) return <p>Error</p>;
    		      if(data){
    		        return <div> Helllo {data.candidates[0].lastName} </div> 
    		      }
    		     
    		      }}
    		</Query>
    		
    		</ApolloProvider>
    		
    );
  }
}

export default App;
