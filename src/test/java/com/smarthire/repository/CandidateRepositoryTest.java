package com.smarthire.repository;

import static org.junit.Assert.assertEquals;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.smarthire.model.Candidate;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CandidateRepositoryTest {

	@Autowired
	private CandidateRepository candidateRepository;

	@Test
	public void test_findAllMethod() {

		Candidate candidate = new Candidate();
		String firstName = "Krishna";
		String lastName = "Lahoti";
		boolean isActive = true;
		String contactNumber = "9404975733";
		String emailId = "krishlahoti@gmail.com";
		String referredByEmployeeId = "2K1H";
		String referredByEmployeeName = "Prasad";

		candidate.setFirstName(firstName);
		candidate.setLastName(lastName);
		candidate.setActive(isActive);
		candidate.setContactNumber(contactNumber);
		candidate.setEmailId(emailId);
		candidate.setReferredByEmployeeId(referredByEmployeeId);
		candidate.setReferredByEmployeeName(referredByEmployeeName);

		candidateRepository.save(candidate);

		List<Candidate> candidateList = candidateRepository.findAll();

		assertEquals(1, candidateList.size());
		assertEquals(firstName, candidateList.get(0).getFirstName());
		assertEquals(isActive, candidateList.get(0).isActive());
		assertEquals(lastName, candidateList.get(0).getLastName());
		assertEquals(contactNumber, candidateList.get(0).getContactNumber());
		assertEquals(emailId, candidateList.get(0).getEmailId());
		assertEquals(referredByEmployeeId, candidateList.get(0).getReferredByEmployeeId());
		assertEquals(referredByEmployeeName, candidateList.get(0).getReferredByEmployeeName());

	}
}
