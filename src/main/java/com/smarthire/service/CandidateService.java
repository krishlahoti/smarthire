package com.smarthire.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.smarthire.model.Candidate;
import com.smarthire.repository.CandidateRepository;

@Service
@Transactional
public class CandidateService {
	private CandidateRepository candidateRepository;

    //In this service we never delete a candidate	

	@Autowired
	public CandidateService(CandidateRepository candidateRepository) {
		this.candidateRepository = candidateRepository;
	}

	public Optional<Candidate> getCandidate(int id) {
		return this.candidateRepository.findById(id);
	}

	public List<Candidate> getAllCandidates() {
		return this.candidateRepository.findAll();
	}

	public Candidate newCandidate(Candidate candidate) {
		return this.candidateRepository.save(candidate);
	}

	public Candidate updateCandidate(Candidate candidate) {
		return this.candidateRepository.save(candidate);
	}

}
