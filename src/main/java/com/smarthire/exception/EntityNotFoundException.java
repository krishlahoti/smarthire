package com.smarthire.exception;

public class EntityNotFoundException extends Exception{

	private static final long serialVersionUID = 1L;

	public EntityNotFoundException(String entityName, int id) {
		super( entityName + "  not found for '" + id + "'");	
	}	
}
