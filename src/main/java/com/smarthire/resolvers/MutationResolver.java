package com.smarthire.resolvers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.smarthire.model.Candidate;

import com.smarthire.service.CandidateService;

@Component
public class MutationResolver implements GraphQLMutationResolver {

	@Autowired
	private CandidateService candidateService;

	public Candidate createCandidate(Candidate candidate) {
		return this.candidateService.newCandidate(candidate);
	}

	public Candidate updateCandidate(Candidate candidate) {
		return candidateService.updateCandidate(candidate);
	}
}
