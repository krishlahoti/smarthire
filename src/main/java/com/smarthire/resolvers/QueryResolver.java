package com.smarthire.resolvers;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.smarthire.model.Candidate;

import com.smarthire.service.CandidateService;

@Component
public class QueryResolver implements GraphQLQueryResolver {

	private CandidateService candidateService;

	public QueryResolver(CandidateService candidateService) {
		this.candidateService = candidateService;
	}

	public List<Candidate> getAllCandidates() {
		return this.candidateService.getAllCandidates();
	}

	public Optional<Candidate> getCandidate(int id) {
		return this.candidateService.getCandidate(id);
	}

}
