package com.smarthire.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.smarthire.model.Candidate;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Integer>{
	

}
