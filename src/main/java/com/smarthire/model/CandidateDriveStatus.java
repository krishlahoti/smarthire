package com.smarthire.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CANDIDATE_DRIVE_STATUS")
public class CandidateDriveStatus {
	
	@EmbeddedId
    private DriveCandidateKey driveCandidateKey;
	
	@Column(name = "INTERVIEW_ROUND_NAME", nullable = false)
	private String interviewRoundName;

	@Column(name = "ROUND_STATUS", nullable = false)
	private RoundStatus roundStatus;

	public DriveCandidateKey getDriveCandidateKey() {
		return driveCandidateKey;
	}

	public void setDriveCandidateKey(DriveCandidateKey driveCandidateKey) {
		this.driveCandidateKey = driveCandidateKey;
	}

	public int getCandidateId() {
		return driveCandidateKey.getCandidateId();
	}

	public void setCandidateId(int candidateId) {
		this.driveCandidateKey.setCandidateId(candidateId);
	}

	public int getDriveId() {
		return driveCandidateKey.getDriveId();
	}

	public void setDriveId(int driveId) {
		this.driveCandidateKey.setDriveId(driveId);
	}

	public String getInterviewRoundName() {
		return interviewRoundName;
	}

	public void setInterviewRoundName(String interviewRoundName) {
		this.interviewRoundName = interviewRoundName;
	}

	public RoundStatus getRoundStatus() {
		return roundStatus;
	}

	public void setRoundStatus(RoundStatus roundStatus) {
		this.roundStatus = roundStatus;
	}

}
