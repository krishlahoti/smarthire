package com.smarthire.model;

public enum RoundStatus {
	CLEARED,
	ON_HOLD,
	IN_PROGRESS, //candidate's interview is in progress
	REJECTED
}
