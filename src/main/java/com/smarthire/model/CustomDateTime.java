package com.smarthire.model;

import java.io.Serializable;

public class CustomDateTime implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	//TODO: Decide the date time api
	private String date;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}	
	
}
