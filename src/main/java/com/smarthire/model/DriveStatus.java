package com.smarthire.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/*
 * This table used to maintain history of progress of candidate
 * */


@Entity
@Table(name = "DRIVE_STATUS")
public class DriveStatus {
	
    @EmbeddedId
    private DriveCandidateKey driveCandidateKey;
	
	@Column(name = "INTERVIEW_ROUND_NAME", nullable = false)
	private String interviewRoundName;

	@Column(name = "INTERVIEWER_ID", nullable = false)
	private int interviewerId;
	
	@Embedded
	@Column(name = "UPDATED_TIME", nullable = false)
	private CustomDateTime updatedTime;

	@Column(name = "ROUND_STATUS", nullable = false)
	private RoundStatus roundStatus;

	@Column(name = "COMMENTS", nullable = false)
	private String comments;

	
	public DriveCandidateKey getDriveCandidateKey() {
		return driveCandidateKey;
	}

	public void setDriveCandidateKey(DriveCandidateKey driveCandidateKey) {
		this.driveCandidateKey = driveCandidateKey;
	}

	public int getCandidateId() {
		return driveCandidateKey.getCandidateId();
	}

	public void setCandidateId(int candidateId) {
		this.driveCandidateKey.setCandidateId(candidateId);
	}

	public int getDriveId() {
		return driveCandidateKey.getDriveId();
	}

	public void setDriveId(int driveId) {
		this.driveCandidateKey.setDriveId(driveId);
	}
	
	public String getInterviewRoundName() {
		return interviewRoundName;
	}

	public void setInterviewRoundName(String interviewRoundName) {
		this.interviewRoundName = interviewRoundName;
	}

	public int getInterviewerId() {
		return interviewerId;
	}

	public void setInterviewerId(int interviewerId) {
		this.interviewerId = interviewerId;
	}

	public CustomDateTime getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(CustomDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}

	public RoundStatus getRoundStatus() {
		return roundStatus;
	}

	public void setRoundStatus(RoundStatus roundStatus) {
		this.roundStatus = roundStatus;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
