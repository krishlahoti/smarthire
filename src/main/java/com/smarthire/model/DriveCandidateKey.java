package com.smarthire.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DriveCandidateKey implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Column(name = "CANDIDATE_ID", nullable = false)
	private int candidateId;

	@Column(name = "DRIVE_ID", nullable = false)
	private int driveId;

	public int getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(int candidateId) {
		this.candidateId = candidateId;
	}

	public int getDriveId() {
		return driveId;
	}

	public void setDriveId(int driveId) {
		this.driveId = driveId;
	}
	
	

}
