
package com.smarthire.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "INTERVIEW_ROUND")
public class InterviewRound {

	@Id
	@GeneratedValue
	@Column(name = "ID", nullable = false)
	public int id;

	@Column(name = "NAME", nullable = false)
	public String name;

	@Column(name = "DRIVE_ID", nullable = false)
	public int driveId;

	public InterviewRound() {
	}

	public InterviewRound(int id, String name, int driveId) {
		super();
		this.id = id;
		this.name = name;
		this.driveId = driveId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDriveId() {
		return driveId;
	}

	public void setDriveId(int driveId) {
		this.driveId = driveId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(driveId, id, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof InterviewRound))
			return false;
		InterviewRound other = (InterviewRound) obj;
		return driveId == other.driveId && id == other.id && Objects.equals(name, other.name);
	}

}