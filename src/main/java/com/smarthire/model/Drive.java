package com.smarthire.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DRIVE")
public class Drive {

	// candidate Id (auto-generated)
	@Id
	@GeneratedValue
	@Column(name = "ID", nullable = false)
	private int id;

	// first name of the candidate
	@Column(name = "DESCRIPTION", length = 64, nullable = false)
	private String description;

	@Embedded
	@Column(name = "DATE", length = 64, nullable = false)
	private CustomDateTime date;
	
	@Column(name = "VENUE", length = 64, nullable = false)
	private String venue;
	
	@Column(name = "ORGANIZER", length = 64, nullable = false)
	private String organizer;
	
	@Column(name = "IS_ACTIVE", nullable = false)
	private boolean isActive;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public CustomDateTime getDate() {
		return date;
	}

	public void setDate(CustomDateTime date) {
		this.date = date;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getOrganizer() {
		return organizer;
	}

	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}
	
	//TODO: have a look at this get method for isActive
	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
}
