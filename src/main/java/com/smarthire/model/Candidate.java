package com.smarthire.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CANDIDATE")
public class Candidate {

	// candidate Id (auto-generated)
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="candidate_seq")
	@SequenceGenerator(name="candidate_seq", sequenceName="CANDIDATE_SEQ")
	private int id;

	// first name of the candidate
	@Column(name = "FIRST_NAME", length = 64, nullable = false)
	private String firstName;

	// last name of the candidate
	@Column(name = "LAST_NAME", length = 64, nullable = false)
	private String lastName;

	// email id of the candidate
	@Column(name = "EMAIL_ID", length = 64, nullable = false)
	private String emailId;

	// contact number of the candidate
	@Column(name = "CONTACT_NUMBER", length = 13, nullable = false)
	private String contactNumber;

	// boolean to determine if the candidate is active
	@Column(name = "IS_ACTIVE", nullable = false)
	private boolean isActive;

	// employee id of the candidate referrer
	@Column(name = "REFERRED_BY_EMPLOYEE_ID", length = 10)
	private String referredByEmployeeId;

	// employee id of the candidate referrer
	@Column(name = "REFERRED_BY_EMPLOYEE_NAME", length = 64)
	private String referredByEmployeeName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getReferredByEmployeeId() {
		return referredByEmployeeId;
	}

	public void setReferredByEmployeeId(String referredByEmployeeId) {
		this.referredByEmployeeId = referredByEmployeeId;
	}

	public String getReferredByEmployeeName() {
		return referredByEmployeeName;
	}

	public void setReferredByEmployeeName(String referredByEmployeeName) {
		this.referredByEmployeeName = referredByEmployeeName;
	}

}
